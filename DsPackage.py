# -*- coding: utf-8 -*-
"""
Created on Tue Aug 25 11:33:15 2015

@author: fg086897
"""

__author__ = 'Neil Wang'

import ccitt_crc

class DsPackage(object):    
    
    access = 'reg'    
    
    def __init__(self, rw, access, address, length):
        self.rw = rw
        self.access = access
        self.address = address
        self.length = length
    
    def generatePackage(self):        
        package = [0 for n in range(8)]
        package[0] = 0xAA
        if self.rw.lower() == 'r' :
            package[1] &= 0x3F
        elif self.rw.lower() == 'w':
            package[1] |= 0x80
            package.append([0 for n in range(self.length)])
        else :
            print('Please input access as "r" or "w"')
            pass

        if self.access == 'reg':
            package[1] += 0x01            
        elif self.access == 'data':
            package[1] += 0x03
        else :
            print('Please input access as "reg" or "data"')
            pass
        package[2] = self.address & 0xff
        package[3] = (self.address >> 8) & 0xff
        package[4] = self.length & 0xff
        package[5] = (self.length >> 8 ) & 0xff
        
        crc = ccitt_crc.ccitt_crc();
        crcValue = crc.ccitt_crc_calc(package[1:-2])
        package[-1] = (crcValue >> 8 ) & 0xff
        package[-2] = crcValue & 0xff
        return package
        
