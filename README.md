#DreamSerial

###I want to build this serial tool to make my serial debug much easier than before.

1. Search available serial device. 
2. Simply transmit and receive handler.
3. Able to send HEX/String, Show HEX/STRING/ASCII/BIN.
4. User can write simple script to send self-define protocols.
5. enhanced CRC module available.
6. When usb-ttl un-plug, the program will not cause any system failure.
7. Save user command history in a list.
8. Show in a grid view, or something like that.

####Script Function
user can use Python to write simple script
use script to analyze the receive data or generate tx data
use script to plot the data with QwtPlot or other tool.




#### Developing Notes
How to transfer PyQt .ui file to .py file.
Use command as following:
c:\Python34\Lib\site-packages\PyQt5\pyuic5 -x ..\DreamSerial\UI\dreamSerialMainForm.ui -o DreamSerial_Ui.py

