# -*- coding: utf-8 -*-

__author__ = 'fg086897'


def HexString2ByteArray(hexString):
    byteStrArray = [int(x, 16) for x in hexString.replace('0x', '').split()]
    return byteStrArray


def checkData(data, _type):
    if "" == data:
        return False, u"Data can't be NULL"

    errch, msg = None, 'Success'
    if 'hex' == _type:
        data = ''.join(data.split())
        if 0 != len(data) / 2:
            errch, msg = True, 'Data Length Error'
        else:
            for ch in data.upper():
                if not ('0' <= ch <= '9' or 'A' <= ch <= 'F'):
                    errch, msg = ch, 'Error Hex Char'
                    break;
    return not errch, msg


def getByteArray(data, _type):
    if 'hex' == _type:
        return HexString2ByteArray(data)
    else:
        return bytes(data, 'utf-8')
