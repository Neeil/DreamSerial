# -*- coding: utf-8 -*-

__author__ = 'fg086897'


import logging
from ccitt_crc import ccitt_crc
import binascii

logger = logging.getLogger('main.TinaUtils')

class TinaUtils():

    def __init__(self):
        logger.info('TinaUtils Init')

    def generateReadRegsCommand(self,startAddress, length):
        addressL = startAddress & 0xFF
        addressH = (startAddress >> 8 ) & 0xFF

        lengthL = length & 0xFF
        lengthH= (length >> 8 ) & 0xFF
        tinaCmd = [0xAA, 0x01, addressL, addressH, lengthL, lengthH]

        ccittCrc = ccitt_crc()
        crc = ccittCrc.ccitt_crc_calc(tinaCmd[1:])
        tinaCmd.append(crc & 0xFF)
        tinaCmd.append((crc >> 8) & 0xFF)
        # tinaHex = [hex(x) for x in tinaCmd]
        # logger.info(tinaHex)
        return tinaCmd